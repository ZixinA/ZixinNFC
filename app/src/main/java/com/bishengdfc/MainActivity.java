package com.bishengdfc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.editText)
    EditText chooseviewtext;
    @OnClick(R.id.button) void submit() {
        startActivityForResult(new Intent(MainActivity.this, RFIDReaerActivity.class), 10);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 10 && resultCode == 1) {
            chooseviewtext.setText(SystemStatus.RFID_Code);
        }
    }
}
