package com.bishengdfc;

/**
 * Created by user on 2016/4/4.
 */
public class SystemStatus {
    public static final String TAG = "Cruise";
    public static MainActivity MainActivity;
    public static String WEBSERVICE_SERVER_ADDRESS = "";
    public static final int DatabaseVersion = 2;
    public static String DATABASE_NAME = "cruise.db";
    public static int RequestCommitStep = 5;
    public static int ForeverRequestCommitStep = 120;
    public static String RFID_Code = "";
    public static String VERSION = "";
    public static int OperationType = 0;
    public static int SToast_ShowTime = 1500;
    /**
     * 系统数据路径
     */
    /**
     * 系统配置文件路径
     */
    /**
     * 配置文件标记
     */
    public static final String CONFIG_SECTION = "Configs";

}
